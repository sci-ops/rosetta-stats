### Hide code chunks through the book (can be
### overridden on a chunk-by-chunk basis)
knitr::opts_chunk$set(echo = FALSE);
maxWidth <- 10