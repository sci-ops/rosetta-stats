---
output: html_document
editor_options: 
  chunk_output_type: console
---

# Aggregating data: mean {#aggregating-data-mean}

## Intro

A common task is aggregating multiple variables (columns in a dataset) into one new variable (column). For example, you may want to compute the average score on the items of a questionnaire.

Note that when creating new variable names, it is important to follow the convention for variable names (see section  \ref(software-basics-file-and-variable-name-conventions)).

### Example dataset

This example uses the Rosetta Stats example dataset "pp15" (see Chapter \@ref(datasets) for information about the datasets and Chapter \@ref(loading-data) for an explanation of how to load datasets).

### Variable(s)

```{r 150-aggregating-data-mean-define-backtick}
backtick <- "`";
```

From this dataset, this example uses variables `r ufs::vecTxt(grep("highDose_AttGeneral", names(rosetta::pp15), value=TRUE), useQuote=backtick);`.

We will aggregate these into the variable `highDose_attitude` (note that this variable already exists in the dataset, and that existing variable is also the mean of those five variables).

## Input: jamovi

In the "Data" tab, click the "Compute" button as shown in Figure \@ref(fig:150-aggregating-data-mean-jamovi-1).

```{r 150-aggregating-data-mean-jamovi-1, fig.cap='Aggregating in jamovi: opening Compute menu'}
knitr::include_graphics(here::here("img","150-recoding-data-invert-jamovi-8.png"));
```

Type in the new variable name in the text field at the top, labelled "COMPUTED VARIABLE". Then click the function button, marked $f_x$, select the MEAN function from the box labelled "Functions", and double click all variables for which you want the mean in the box labelled "Variables", while typing a comma in between each variable name as shown in Figure \@ref(fig:150-aggregating-data-mean-jamovi-1).

```{r 150-aggregating-data-mean-jamovi-2, fig.cap='Aggregating in jamovi: using the function menu to specify a computation'}
knitr::include_graphics(here::here("img","150-aggregating-data-mean-jamovi-1.png"));
```

Alternatively, you can type the function name and list of variables directly without using the function ($f_x$) dialog as shown in Figure \@ref(fig:150-aggregating-data-mean-jamovi-2).

```{r 150-aggregating-data-mean-jamovi-3, fig.cap='Aggregating in jamovi: directly typing in a computation'}
knitr::include_graphics(here::here("img","150-aggregating-data-mean-jamovi-2.png"));
```

If you want to allow missing values, you can specify the `ignore_missing=1` argument. In that case, you would type:

```r
MEAN(highDose_AttGeneral_good, highDose_AttGeneral_prettig,
     highDose_AttGeneral_slim, highDose_AttGeneral_gezond,
     highDose_AttGeneral_spannend, ignore_missing=1)
```

It is as yet not possible to indicate the number of valid values that is required; either no missings are allowed at all, or any number of missing values is accepted.

## Input: R

In R, there are roughly three approaches. Many analyses can be done with base R without installing additional packages. The `rosetta` package accompanies this book and aims to provide output similar to jamovi and SPSS with simple commands. Finally, the tidyverse is a popular collection of packages that try to work together consistently but implement a different underlying logic that base R (and so, the `rosetta` package).

### R: base R

```r
dat$highdose_attitude <-
  rowMeans(
    dat[,
      c(
        'highDose_AttGeneral_good',
        'highDose_AttGeneral_prettig',
        'highDose_AttGeneral_slim',
        'highDose_AttGeneral_gezond',
        'highDose_AttGeneral_spannend'
      )
    ]
  );
```

### R: Rosetta

```r
dat$highdose_attitude <-
  rosetta::means(
    data = dat,
    'highDose_AttGeneral_good',
    'highDose_AttGeneral_prettig',
    'highDose_AttGeneral_slim',
    'highDose_AttGeneral_gezond',
    'highDose_AttGeneral_spannend'
  );
```

To indicate that a certain number of values must be valid (i.e. "non-missing"), the argument `requiredValidValues` can be passed. For example, to require four valid values (instead of requiring only one valid value, the default), use:

```r
dat$highdose_attitude <-
  rosetta::means(
    data = dat,
    'highDose_AttGeneral_good',
    'highDose_AttGeneral_prettig',
    'highDose_AttGeneral_slim',
    'highDose_AttGeneral_gezond',
    'highDose_AttGeneral_spannend',
    requiredValidValues = 4
  );
```

## Input: SPSS

For SPSS, there are two approaches: using the Graphical User Interface (GUI) or specify an analysis script, which in SPSS are called "syntax".

### SPSS: GUI

First activate the `dat` dataset (see \@ref(software-basics-spss-active-dataset)).

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

### SPSS: Syntax

```
COMPUTE highdose_attitude =
  MEAN(
    highDose_AttGeneral_good,
    highDose_AttGeneral_prettig,
    highDose_AttGeneral_slim,
    highDose_AttGeneral_gezond,
    highDose_AttGeneral_spannend
  ).
```

To indicate that a certain number of values must be valid (i.e. "non-missing"), the command `MEAN` can be appended with a period and the number of required valid values. For example, to required four valid values, use:

```
COMPUTE highdose_attitude =
  MEAN.4(
    highDose_AttGeneral_good,
    highDose_AttGeneral_prettig,
    highDose_AttGeneral_slim,
    highDose_AttGeneral_gezond,
    highDose_AttGeneral_spannend
  ).
```

## Output

Aggregating variables is not an analysis, and as such, does not produce output. You can inspect the newly created variable to ensure it has been created properly.
